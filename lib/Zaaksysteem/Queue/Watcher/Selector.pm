package Zaaksysteem::Queue::Watcher::Selector;

use Moose::Role;

use IO::Select;

=head1 NAME

Zaaksysteem::Queue::Watcher::Selector - IO::Select role for the queue watcher

=head1 DESCRIPTION

This role, when applied, adds a L</selector> attribute to the class/instance.

=head1 DEPENDENCIES

=head2 dbh_sockets

This method should return a list of filehandles to be listened on.

=cut

requires qw[dbh_sockets];

=head1 ATTRIBUTES

=head2 selector

A convenience attribute that holds a reference to a L<IO::Select> instance.

The instance is build via the C<_build_selector> method, which can be
overridden in the consuming class.

=head3 Proxied methods

=over 4

=item can_read

See L<IO::Select/can_read>.

=item has_sockets

See L<IO::Select/count>.

=item remove_socket

See L<IO::Select/remove>.

=back

=cut

has selector => (
    is => 'ro',
    isa => 'IO::Select',
    lazy => 1,
    builder => '_build_selector',
    clearer => 'clear_selector',
    handles => {
        'can_read' => 'can_read',
        'has_sockets' => 'count',
        'remove_socket' => 'remove',
    }
);

sub _build_selector {
    my $self = shift;

    return IO::Select->new($self->dbh_sockets);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
