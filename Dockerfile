# Build this using: docker build --label=zaaksysteem/zs-queue:latest .
FROM registry.gitlab.com/zaaksysteem/platform:f9919224

ENV SERVICE_HOME /opt/zaaksysteem-queue-runner

WORKDIR $SERVICE_HOME

COPY Makefile.PL ./
COPY bin ./bin
COPY lib ./lib

RUN    useradd -ms /bin/bash zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem

# Install dependencies
RUN    apt-get update \
    && apt-get -y install libpq-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN cpanm --installdeps . && rm -rf || (cat ~/.cpanm/work/*/build.log && /usr/bin/false)

RUN perl Makefile.PL && make install

USER zaaksysteem

CMD [ "zs-queue.pl", "-d", "/etc/zaaksysteem/zaaksysteem.conf" ]
